<?php
	$mysqli = new mysqli('localhost', 'laa', '****', 'laa2746'); 
	if(mysqli_connect_errno())
	{
	   printf("Подключение к серверу MySQL невозможно. Код ошибки: %s\n", mysqli_connect_error());
	   exit;
	}
	if($_GET['author'] == 'all')
	{
		echo get_Authors($mysqli);
	} else if($_GET['author'])
	{
		$author = $_GET['author'];
		$author = preg_replace("/[^0-9a-z ]/i","",(string)$author);
		echo get_Author($mysqli, $author);
		
	} else if($_GET['include_daughter'])
	{
		$heading = $_GET['heading'];
		$heading = preg_replace("/[^0-9a-z ]/i","",(string)$heading);
		echo get_Headings($mysqli, $heading);
		
	} else if($_GET['heading'])
	{
		$heading = $_GET['heading'];
		$heading = preg_replace("/[^0-9a-z ]/i","",(string)$heading);
		echo get_Heading($mysqli, $heading);
		
	} else if($_GET['title'])
	{
		$title = $_GET['title'];
		$title = preg_replace("/[^0-9a-z ]/i","",(string)$title);
		echo get_Title($mysqli, $title);
		
	} else if($_GET['id'])
	{
		$id = $_GET['id'];
		$id = preg_replace("/[^0-9]/i","",(string)$id);
		echo get_ID($mysqli, $id);

	}
	
	$mysqli->close();
	
	function get_ID_headings($mysqli, $name, $parents_str = "")
	{
		if($parents_str == "")
		{
			$parents_str = "h.name = '" . $name . "' ";
		}
		$stmt = $mysqli->query("SELECT h.name FROM headings h INNER JOIN headings hr ON hr.id = h.id_high WHERE hr.name = '$name'");
		while( $row = mysqli_fetch_assoc($stmt) )
		{
			$parents_str = $parents_str . "OR " . "h.name = '" . $row['name'] . "' ";
			$parents_str = get_ID_headings($mysqli, $row['name'], $parents_str);
		}
		$stmt->close();
		return $parents_str;
	}
	
	function get_Authors($mysqli)
	{
		$stmt = $mysqli->query("SELECT * FROM autors");
		while( $row = mysqli_fetch_assoc($stmt) )
		{
			$authors[$row['id']]['fio'] = $row['fio'];
			$authors[$row['id']]['avatar'] = $row['avatar'];
			$authors[$row['id']]['signature'] = $row['signature'];
		}
		$stmt->close();
		return json_encode($authors);
	}
	
	function get_Author($mysqli, $author)
	{
		$stmt = $mysqli->query("SELECT n.title, n.preview, n.text, n.id, h.name FROM news n INNER JOIN newstoheadings nh ON n.id = nh.id_news INNER JOIN headings h ON h.id = nh.id_heading INNER JOIN autors a ON n.id_autor = a.id WHERE a.fio = '$author'");
		while( $row = mysqli_fetch_assoc($stmt) )
		{
			$news[$row['id']]['title'] = $row['title'];
			$news[$row['id']]['preview'] = $row['preview'];
			$news[$row['id']]['text'] = $row['text'];
			$news[$row['id']]['headings'][] = $row['name'];
		}
		$stmt->close();
		$ret = array();
		$i = 0;
		foreach($news as $one_news)
		{
			$i++;
			$ret[$i] = $one_news;
		}
		return json_encode($ret);
	}
	
	function get_Headings($mysqli, $heading)
	{
		$headings = get_ID_headings($mysqli, $heading);
		$stmt = $mysqli->query("SELECT n.title, n.preview, n.text, n.id, a.fio FROM news n INNER JOIN newstoheadings nh ON n.id = nh.id_news INNER JOIN headings h ON h.id = nh.id_heading INNER JOIN autors a ON n.id_autor = a.id WHERE $headings");
		while( $row = mysqli_fetch_assoc($stmt) )
		{
			$news[$row['id']]['title'] = $row['title'];
			$news[$row['id']]['preview'] = $row['preview'];
			$news[$row['id']]['text'] = $row['text'];
			$news[$row['id']]['fio'] = $row['fio'];
		}
		$stmt->close();
		$ret = array();
		$i = 0;
		foreach($news as $one_news)
		{
			$i++;
			$ret[$i] = $one_news;
		}
		return json_encode($ret);
		
	}
	
	function get_Heading($mysqli, $heading)
	{
		$stmt = $mysqli->query("SELECT n.title, n.preview, n.text, n.id, a.fio FROM news n INNER JOIN newstoheadings nh ON n.id = nh.id_news INNER JOIN headings h ON h.id = nh.id_heading INNER JOIN autors a ON n.id_autor = a.id WHERE h.name = '$heading'");
		while( $row = mysqli_fetch_assoc($stmt) )
		{
			$news[$row['id']]['title'] = $row['title'];
			$news[$row['id']]['preview'] = $row['preview'];
			$news[$row['id']]['text'] = $row['text'];
			$news[$row['id']]['fio'] = $row['fio'];
		}
		$stmt->close();
		$ret = array();
		$i = 0;
		foreach($news as $one_news)
		{
			$i++;
			$ret[$i] = $one_news;
		}
		return json_encode($ret);
		
	}
	
	function get_Title($mysqli, $title)
	{
		$stmt = $mysqli->query("SELECT n.title, n.preview, n.text, n.id, a.fio, h.name FROM news n INNER JOIN newstoheadings nh ON n.id = nh.id_news INNER JOIN headings h ON h.id = nh.id_heading INNER JOIN autors a ON n.id_autor = a.id WHERE n.title = '$title'");
		while( $row = mysqli_fetch_assoc($stmt) )
		{
			$news[1]['title'] = $row['title'];
			$news[1]['preview'] = $row['preview'];
			$news[1]['text'] = $row['text'];
			$news[1]['fio'] = $row['fio'];
			$news[1]['headings'] = $row['name'];
		}
		$stmt->close();
		return json_encode($news);
		
	}
	
	function get_ID($mysqli, $id)
	{
		$stmt = $mysqli->query("SELECT n.title, n.preview, n.text, n.id, a.fio, h.name FROM news n INNER JOIN newstoheadings nh ON n.id = nh.id_news INNER JOIN headings h ON h.id = nh.id_heading INNER JOIN autors a ON n.id_autor = a.id WHERE n.id = $id");
		while( $row = mysqli_fetch_assoc($stmt) )
		{
			$news[1]['title'] = $row['title'];
			$news[1]['preview'] = $row['preview'];
			$news[1]['text'] = $row['text'];
			$news[1]['fio'] = $row['fio'];
			$news[1]['headings'] = $row['name'];
		}
		$stmt->close();
		return json_encode($news);
	}
?>

