Use laa2746;

DROP TABLE IF EXISTS `autors`;
CREATE TABLE `autors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fio` varchar(50) NOT NULL,
  `avatar` varchar(50) DEFAULT NULL,
  `signature` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `headings`;
CREATE TABLE `headings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_high` int(11) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_high` (`id_high`),
  CONSTRAINT `headings_ibfk_1` FOREIGN KEY (`id_high`) REFERENCES `headings` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `preview` varchar(200) NOT NULL,
  `text` varchar(2000) NOT NULL,
  `id_autor` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_autor` (`id_autor`),
  CONSTRAINT `news_ibfk_1` FOREIGN KEY (`id_autor`) REFERENCES `autors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `newstoheadings`;
CREATE TABLE `newstoheadings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_news` int(11) NOT NULL,
  `id_heading` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `onlyNews` (`id_news`),
  KEY `onlyHeadings` (`id_heading`),
  CONSTRAINT `onlyHeadings` FOREIGN KEY (`id_heading`) REFERENCES `headings` (`id`),
  CONSTRAINT `onlyNews` FOREIGN KEY (`id_news`) REFERENCES `news` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
