<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Новости</title>
	</head>
	<body>
		<div>
			Автор: <input id="author">
			<button id="author_butt" onclick="one_author();">Вывод новостей одного автора</button>
		</div>
		<div>
			Рубрика: <input id="heading">
			Вывод из рубрики <button id="heading_butt1" onclick="one_heading_daughter();">включая дочерние</button><button id="heading_butt2" onclick="one_heading();">не включая дочерние</button>			
		</div>
		<div>
			Вывод списка авторов: 
			<button id="all_authors" onclick="all_authors();">Вывод всех авторов</button>
		</div>
		<div>
			Новость: <input id="news">
			<button id="news_butt1" onclick="one_news_title();">По названию</button><button id="news_butt2" onclick="one_news_id();">По идентификатору</button>
		</div>
		<div id="info"></div>
		<script>
			function ajax(url, callback) {
				var xhr = new XMLHttpRequest();
				xhr.open('GET', url);
				xhr.onreadystatechange = function(){
					if (this.readyState == 4) {
						if (this.status == 200)
							callback(JSON.parse(this.responseText));
					}
				};
				xhr.send(null);
			}
			
			function all_authors() {
				ajax('http://laa.zzz.com.ua/ajax.php?author=all', function(data){
					var i = 1;
					var infoDiv = document.getElementById('info');
					var text = "";
					while(data[i] != undefined)
					{
						text = text + '<div><b>ФИО: ' + data[i].fio + '</b></div>' + '<img src="' + data[i].avatar + '">' + '<div><i>' + data[i].signature + '</i></div>';
						i++;
					}
					infoDiv.innerHTML = text;
				});
			}
			
			function one_author() {
				var author = document.getElementById('author').value;
				if(author)
				{
					ajax('http://laa.zzz.com.ua/ajax.php?author=' + author, function(data){
						var i = 1;
						var infoDiv = document.getElementById('info');
						var text = "";
						while(data[i] != undefined)
						{
							text = text + '<div><b>Заголовок: ' + data[i].title + '</b></div>' + '<div>Заголовок: ' + data[i].preview + '</div>' + '<div><i>' + data[i].text + '</i></div>' + '<div><i>' + data[i].headings + '</i></div>';
							i++;
						}
						infoDiv.innerHTML = text;
						});
				}
				else
					alert('Не выбран автор');

			}
			
			function one_heading() {
				var heading = document.getElementById('heading').value;
				if(heading)
				{
					ajax('http://laa.zzz.com.ua/ajax.php?heading=' + heading, function(data){
						var i = 1;
						var infoDiv = document.getElementById('info');
						var text = "";
						while(data[i] != undefined)
						{
							text = text + '<div><b>Заголовок: ' + data[i].title + '</b></div>' + '<div>Заголовок: ' + data[i].preview + '</div>' + '<div><i>' + data[i].text + '</i></div>' + '<div><i>' + data[i].fio + '</i></div>';
							i++;
						}
						infoDiv.innerHTML = text;
					});
				}
				else
					alert('Не выбрана рубрика');

			}
			
			function one_heading_daughter() {
				var heading = document.getElementById('heading').value;
				if(heading)
				{
					ajax('http://laa.zzz.com.ua/ajax.php?include_daughter=y&heading=' + heading, function(data){
						var i = 1;
						var infoDiv = document.getElementById('info');
						var text = "";
						while(data[i] != undefined)
						{
							text = text + '<div><b>Заголовок: ' + data[i].title + '</b></div>' + '<div>Заголовок: ' + data[i].preview + '</div>' + '<div><i>' + data[i].text + '</i></div>' + '<div><i>' + data[i].fio + '</i></div>';
							i++;
						}
						infoDiv.innerHTML = text;
					});
				}
				else
					alert('Не выбрана рубрика');

			}
			
			function one_news_title() {
				var title = document.getElementById('news').value;
				if(title)
				{
					ajax('http://laa.zzz.com.ua/ajax.php?title=' + title, function(data){
						var i = 1;
						var infoDiv = document.getElementById('info');
						var text = "";
						while(data[i] != undefined)
						{
							text = text + '<div><b>Заголовок: ' + data[i].title + '</b></div>' + '<div>Заголовок: ' + data[i].preview + '</div>' + '<div><i>' + data[i].text + '</i></div>' + '<div><i>' + data[i].fio + '</i></div>' + '<div><i>' + data[i].headings + '</i></div>';
							i++;
						}
						infoDiv.innerHTML = text;
					});
				}
				else
					alert('Не выбрана новость');

			}
			
			function one_news_id() {
				var id = document.getElementById('news').value;
				if(id)
				{
					ajax('http://laa.zzz.com.ua/ajax.php?id=' + id, function(data){
						var i = 1;
						var infoDiv = document.getElementById('info');
						var text = "";
						while(data[i] != undefined)
						{
							text = text + '<div><b>Заголовок: ' + data[i].title + '</b></div>' + '<div>Заголовок: ' + data[i].preview + '</div>' + '<div><i>' + data[i].text + '</i></div>' + '<div><i>' + data[i].fio + '</i></div>' + '<div><i>' + data[i].headings + '</i></div>';
							i++;
						}
						infoDiv.innerHTML = text;
					});
				}
				else
					alert('Не выбрана новость');

			};
		</script>
	</body>
</html>

